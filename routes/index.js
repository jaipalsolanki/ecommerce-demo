const admin = require('./admin/index')
const web = require('./web/index')

module.exports = [
    admin,
    web
]