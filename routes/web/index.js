const promiseRouter = require('express-promise-router');
const router = promiseRouter();

const WebController = require('./../../controller/index').WebController;
router.get('/web/product', WebController.productList);
router.post('/web/addCart', WebController.addCart);
router.post('/web/checkOut', WebController.checkOut);


module.exports = router;
