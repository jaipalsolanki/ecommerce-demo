const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const { authJWT } = require("./../../middleware");
const multer  = require('multer');
const path = require('path');

let productImage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname,'/../../public/productImage/'))
    },
    filename: function (req, file, cb) {
        let ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
        //   cb(null, file.fieldname + '-' + new Date().getTime() + ext)
        cb(null, 'file' + (Math.random().toString(36) + '00000000000000000').slice(2, 10) + '-' + Date.now() + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
    }
})

let image = multer({
    storage: productImage
});

const AdminController = require('./../../controller/index').AdminController;
router.post('/admin/addProduct',image.single('productImage'), AdminController.addProduct);

module.exports = router;