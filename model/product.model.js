const mongoose = require('mongoose');
const productSchema = new mongoose.Schema({
    productName: {
		type: String,
		required: true
    },
    productImage: {
		type: String,
		required: true
    },
    productPrice: {
		type: Number,
		required: true,
    },
    productDescription: {
		type: String,
		required: true
    },
    productQuantity: {
		type: Number,
		required: true,
    },
},{ timestamps: true });

module.exports = mongoose.model('product', productSchema);
