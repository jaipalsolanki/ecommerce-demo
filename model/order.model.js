const mongoose = require('mongoose');
const orderSchema = new mongoose.Schema({
    totalAmount: {
		type: Number,
		required: true,
    },
    products:{
        type:Array
    },
    Address:{
        type:String
    },
    email:{
        type:String
    },
    name:{
        type:String
    }
},{ timestamps: true });

module.exports = mongoose.model('order', orderSchema);
