'use strict';

const { of } = require('await-of');
const Product = require('./../model/product.model');
const Cart = require('./../model/cart.model');
const Order = require('./../model/order.model');
const path = require('path');
const pdfInvoice = require('pdf-invoice');
const nodemailer = require("nodemailer");

const publicKey = 'pk_test_51HrpQmFh1OoFZF3WVpJUbrkukfE4c64BLc6oWw0nU4QHVMRRMuc2WwQsKGP9wWPn3cVgqk008JVy6i0flhQXhYXK00Pmz74g3a';
const secretkey = 'sk_test_51HrpQmFh1OoFZF3WjZcQzuoKMFUPW8S50y0rx14HP6ZpehVKFsIIz9pRHiw5Flq6fDvrGMTz4r0Zx7ZtAZIOfVEU00AkknFLCu';

var stripe = require('stripe')(secretkey);

  const productList = async (req, res) => {
    const [productData, err] = await of(Product.find().sort({ createdAt: -1 }))
    if (err){
      res.statusCode = 400;
      return res.json({
      message: 'failed'
      });
    }
    res.statusCode = 200;
      return res.json({
        message: 'success',
        data:productData
      });
  }  

  const addCart = async (req, res) => {
    const data = {
      ...req.body
    }
    const [checkQuantity, Checkerr] = await of(Product.findOne({_id:data.productId}));

    if(checkQuantity.productQuantity < data.quantity){
      res.statusCode = 400;
      return res.json({
        message: 'product quantity not found as per your requirement'
      });
    }
    const [result, err] = await of(Cart.findOneAndUpdate({_id:data.productId},data,{upsert: true, new: true}))
    if (err) {
      res.statusCode = 400;
      return res.json({
        message: 'failed'
      });
    }
    res.statusCode = 201;
      return res.json({
        message: 'success'
      });
  }

const checkOut = async (req,res) =>{
  var OBJ = req.body;
  //return
//   stripe.customers.create({ 
//     email: data.email, 
//     source: '', 
//     name: data.name, 
//     address: { 
//         line1: 'TC 9/4 Old MES colony', 
//         postal_code: '452331', 
//         city: 'Indore', 
//         state: 'Madhya Pradesh', 
//         country: 'India', 
//     } 
// }) 
// .then((customer) => { 
//     return stripe.charges.create({ 
//         amount: data.totalAmount,     // Charing Rs 25 
//         description: 'Web Development Product', 
//         currency: 'INR', 
//         customer: customer.id 
//     }); 
// }) 
// .then((charge) => { 
//     res.send("Success")  // If no error occurs 
// }) 
// .catch((err) => { 
//     res.send(err)       // If some error occurs 
//    })
const [result, err] = await of(new Order(OBJ).save());
  if (err) {
    res.statusCode = 400;
    return res.json({
      message: 'failed'
    });
  }

  const document = pdfInvoice(
    {
    company: {
        phone: '(99) 9 9999-9999',
        email: 'company@evilcorp.com',
        address: 'Av. Companhia, 182, Água Branca, Piauí',
        name: 'Evil Corp.',
      },  
    customer: {
      name: OBJ.name,
      email: OBJ.email,
    },
    items: OBJ.products,
    });

  const fs = require('fs');
  document.generate() ;
  let filename = 'orderPDF'+result._id+'.pdf';
  document.pdfkitDoc.pipe(fs.createWriteStream(path.join(__dirname,'/../public/pdf/'+filename)))
   
//   let mailTransporter = nodemailer.createTransport({ 
//     service: 'gmail',
//     auth: { 
//         user: '', 
//         pass: ''
//     } 
// }); 
//   let info = await mailTransporter.sendMail({
//     from: 'j.p.solanki01@gmail.com', // sender address
//     to: "jaipal.solanki@engineerbabu.in", // list of receivers
//     subject: "Hello ✔", // Subject line
//     text: "Hello world?", // plain text body
//     html: "<b>Hello world?</b>", // html body
//   });

  res.statusCode = 201;
      return res.json({
        message: 'Order created successfully'
      });
}  

module.exports = {
    productList,
    addCart,
    checkOut
}  