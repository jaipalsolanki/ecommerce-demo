const { of } = require('await-of');
const Product = require('./../model/product.model');

  const addProduct = async (req, res) => {
    const data = {
      ...req.body
    }

    if(req.file){
       data.productImage = req.file.filename;
    }
  	const [result, err] = await of(new Product(data).save());
    if (err) {
      res.statusCode = 400;
      return res.json({
        message: 'failed'
      });
    }
    res.statusCode = 201;
      return res.json({
        message: 'success'
      });
  }

  module.exports = {
    addProduct
}